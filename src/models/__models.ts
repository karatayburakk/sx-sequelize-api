import { Sequelize } from 'sequelize-typescript';
import Author from './authorModel';
import Book from './bookModel';

export default function defineModels(
  dbConnection: Sequelize,
  cb: (err?: Error) => void
): void {
  dbConnection.addModels([Author, Book]);
  dbConnection
    .sync()
    .then((): void => {
      return cb();
    })
    .catch((e): void => {
      return cb(e);
    });
}
