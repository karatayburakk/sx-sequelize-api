import {
  Table,
  Column,
  Model,
  DataType,
  DefaultScope,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';

import Author from './authorModel';

@DefaultScope(() => ({}))
@Table({
  tableName: 'book',
  modelName: 'book',
  freezeTableName: true,
})
export default class Book extends Model<Book> {
  @Column({
    type: DataType.STRING(128),
    allowNull: false,
    unique: true,
  })
  bookName: string;

  @Column({
    type: DataType.INTEGER,
  })
  pages: number;

  @Column({
    type: DataType.INTEGER,
  })
  year: number;

  @ForeignKey(() => Author)
  @Column({
    type: DataType.INTEGER,
  })
  authorId: number;

  @BelongsTo(() => Author)
  author: Author;
}
