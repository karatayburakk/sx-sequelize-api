import {
  Table,
  Column,
  Model,
  DataType,
  DefaultScope,
  HasMany,
} from 'sequelize-typescript';

import Book from './bookModel';

@DefaultScope(() => ({}))
@Table({
  tableName: 'author',
  modelName: 'author',
  freezeTableName: true,
})
export default class Author extends Model<Author> {
  @Column({
    type: DataType.STRING(128),
    allowNull: false,
    unique: true,
  })
  authorName: string;

  @Column({
    type: DataType.INTEGER,
  })
  age: number;

  @HasMany(() => Book)
  books: Book[];
}
