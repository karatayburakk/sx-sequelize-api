import * as pg from 'pg';
import { getLogger } from 'log4js';
import config from '../config';
const logger = getLogger('DB');

export function createDbIfNotExists(): Promise<boolean> {
  return new Promise<boolean>((resolve, reject): void => {
    let client = new pg.Client({
      host: config.db.host,
      database: 'postgres',
      user: config.db.username,
      password: config.db.password,
      port: config.db.port,
    });

    client
      .connect()
      .then((): void => {
        client
          .query('CREATE DATABASE "' + config.db.database + '"')
          .then((): void => {
            logger.warn('New Database Created');
            return resolve(true);
          })
          .catch((err): void => {
            if (err.message.indexOf('already exists') >= 0)
              return resolve(true);
            logger.error(
              `createDbIfNotExists: DB Create Error: ${err.message}`
            );
            return reject(err.message);
          });
      })
      .catch((err): void => {
        logger.error(`createDbIfNotExists: DB Connect Error: ${err.message}`);
        return reject(err.message);
      });
  });
}
