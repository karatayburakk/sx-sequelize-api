import express from 'express';
import config from './config';
import { createDbIfNotExists } from './db/db';
import { Sequelize } from 'sequelize-typescript';
import * as sequelize from 'sequelize';
import defineModels from './models/__models';
import restApi from './api';

let connection: Sequelize;

const app: express.Application = express();
const PORT: number = config.port || 3000;

app.use(express.json());

createDbIfNotExists()
  .then((): void => {
    connect2Db((): void => {
      defineModels(connection, (err): void => {
        restApi(app, connection);
        app.listen(PORT, () => {
          console.log('Server running');
        });
      });
    });
  })
  .catch((err): void => {
    console.log(`createDbIfNotExists: DB Error. Err: ${err.message}`);
    console.log('Exiting...');
    process.exit(-1);
  });

function connect2Db(cb: () => void): void {
  const Op = sequelize.Op;
  const operatorsAliases = {
    $eq: Op.eq,
    $ne: Op.ne,
    $gte: Op.gte,
    $gt: Op.gt,
    $lte: Op.lte,
    $lt: Op.lt,
    $not: Op.not,
    $in: Op.in,
    $notIn: Op.notIn,
    $is: Op.is,
    $like: Op.like,
    $notLike: Op.notLike,
    $iLike: Op.iLike,
    $notILike: Op.notILike,
    $regexp: Op.regexp,
    $notRegexp: Op.notRegexp,
    $iRegexp: Op.iRegexp,
    $notIRegexp: Op.notIRegexp,
    $between: Op.between,
    $notBetween: Op.notBetween,
    $overlap: Op.overlap,
    $contains: Op.contains,
    $contained: Op.contained,
    $adjacent: Op.adjacent,
    $strictLeft: Op.strictLeft,
    $strictRight: Op.strictRight,
    $noExtendRight: Op.noExtendRight,
    $noExtendLeft: Op.noExtendLeft,
    $and: Op.and,
    $or: Op.or,
    $any: Op.any,
    $all: Op.all,
    $values: Op.values,
    $col: Op.col,
  };

  connection = new Sequelize(
    Object.assign(config.db as any, {
      operatorsAliases,
    })
  );
  connection
    .authenticate()
    .then((): void => {
      cb();
    })
    .catch((err: Error): void => {
      console.log(`DB Connection Error. Err: ${err.message}`);
      console.log('Exiting...');
      process.exit(-1);
    });
}
