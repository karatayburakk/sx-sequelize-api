import { Application } from 'express';
import { Sequelize } from 'sequelize-typescript';
import author from './authorController';
import book from './bookController';
export default function (app: Application, connection: Sequelize): void {
  app.use('/api/author', author(connection));
  app.use('/api/book', book(connection));
}
