import * as express from 'express';
import { ModelRestApi } from 'sx-sequelize-api';
import { Sequelize } from 'sequelize-typescript';
import AuthorModel from '../models/authorModel';
import BookModel from '../models/bookModel';
import { Request, Response, NextFunction } from 'express';

export default function api(connection: Sequelize): express.Router {
  const router: express.Router = express.Router();
  const DbModel = AuthorModel;
  const modelApi = new ModelRestApi(DbModel, connection);

  router.get('/', modelApi.getAll());
  router.get('/count', modelApi.count());
  router.get('/:id', modelApi.getById());

  router.post('/', modelApi.create());

  router.get('/books/:id', (req: Request, res: Response) => {
    let books: string[] = [];
    AuthorModel.findByPk(req.params.id, { include: [BookModel] }).then(
      (author) => {
        author?.books?.forEach((book) => {
          books.push(book.bookName);
        });
        res.status(200).json({
          status: 'success',
          books,
        });
      }
    );
  });

  //   router.post(
  //     '/createBulk',
  //     checkUserAccessRight(DbModel, AccessSubject.Tag, AccessType.Write),
  //     checkCreateRowCountLimit(DbModel, getLimits().limits.tag),
  //     modelApi.createBulk()
  //   );

  router.put('/:id', modelApi.updateById());
  router.delete('/:id', modelApi.deleteById());

  return router;
}
