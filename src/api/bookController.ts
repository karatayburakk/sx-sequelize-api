import * as express from 'express';
import { ModelRestApi } from 'sx-sequelize-api';
import { Sequelize } from 'sequelize-typescript';
import Model from '../models/bookModel';

export default function api(connection: Sequelize): express.Router {
  const router: express.Router = express.Router();
  const DbModel = Model;
  const modelApi = new ModelRestApi(DbModel, connection);

  router.get('/', modelApi.getAll());
  router.get('/count', modelApi.count());
  router.get('/:id', modelApi.getById());

  router.post('/', modelApi.create());

  //   router.post(
  //     '/createBulk',
  //     checkUserAccessRight(DbModel, AccessSubject.Tag, AccessType.Write),
  //     checkCreateRowCountLimit(DbModel, getLimits().limits.tag),
  //     modelApi.createBulk()
  //   );

  router.put('/:id', modelApi.updateById());
  router.delete('/:id', modelApi.deleteById());

  return router;
}
