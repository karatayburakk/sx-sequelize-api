export default {
  // DB Config
  db: {
    dialect: 'postgres',
    host: process.env.POSTGRESQL_HOST || 'localhost',
    username: 'postgres',
    password: 'postgres',
    port: 5432,
    database: 'sample-app4',
    logging: true,
  },

  // App Port
  port: 3000,
};
